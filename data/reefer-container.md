Reefer Container ขนส่งสินค้าด้วยตู้ควบคุมอุณหภูมิ ​
===
จัดเข้าพิกัด 8609.00

Reefer Container คือตู้คอนเทนเนอร์ที่สามารถควบคุมอุณหภูมิภายในตู้ได้ หรือก็คือ ตู้เย็นคอนเทนเนอร์ นั่นเอง ตู้ประเภทนี้เหมาะกับสินค้าที่จำเป็นต้องรักษาอุณหภูมิให้คงที่อยู่เสมอ อย่างเช่น อาหาร, ผัก, ผลไม้ เป็นต้น ตู้คอนเทนเนอร์ที่มีออพชั่นพิเศษแบบนี้ราคาค่า Freight ก็ย่อมจะสูงกว่าตู้คอนเทนเนอร์ปกติเป็นธรรมดา โดยถ้าตู้ปกติราคาเท่าไหร่ก็สามารถเอา 2 คูณเข้าไปคร่าวๆ ได้เลย อย่างไรก็ตาม หากผู้ประกอบการต้องการสอบถามราคาค่า Freight สำหรับ Reefer Container ก็ควรเตรียมข้อมูลให้ครบถ้วนสมบูรณ์ เพราะตู้ไม่ธรรมดาก็ย่อมมีอะไรบางอย่างที่พิเศษกว่า ฉะนั้นข้อมูลจำเป็นที่ต้องเตรียมให้พร้อมในการขอราคาค่า Freight

โดยปกติ​ผู้ประกอบการจำเป็นต้องแจ้งชนิดของสินค้ากับสายเรืออยู่แล้ว​ แต่ยิ่งเป็น​ตู้ Reefer นั้นต้องแจ้งอย่างหลีกเลี่ยงไม่ได้ สินค้าที่จะเข้าตู้ไปต้องเหมาะสมกับการใช้ตู้ด้วย บรรดาสายเรือต่าง ๆ ถึงจะปล่อยตู้สินค้าให้

อุณหภูมิที่ต้องการ

เรื่องอุณหภูมินั้นเกี่ยวข้องกับต้นทุนและความสามารถในการให้บริการของสายเรือ เช่น มีตู้ที่อยู่ในสภาพพร้อมให้บริการหรือไม่ หรือ ค่าไฟที่ต้องใช้ตลอดการขนส่ง เป็นต้​น ​และที่สำคัญ​

อุณหภูมิที่ถูกต้องเหมาะสม​กับสินค้า​ตามที่ผู้ประกอบการแจ้ง จะช่วยรักษาสินค้าไม่ให้​เน่าเสียในระหว่างการขนส่ง​

ท่าเรือส่งออก/นำเข้า

ท่าที่จะใช้บรรจุสินค้ามีผลต่อการบริการอยู่พอสมควร เพราะบางท่าก็มีตู้ควบคุมอุณหภูมิน้อยหรือไม่มีเลย เพราะสิ่งอำนวยความสะดวกที่เรียกว่า “ปลั๊กไฟ” มีให้ใช้งานได้ไม่เพียงพอ และบางครั้งก็อยู่ที่เรือด้วย เรือบางลำก็มีปลั๊กสำหรับตู้ Reefer น้อย บางครั้งต้องมีการสลับกันใช้ปลั๊กบนเรือเลยทีเดียว

​ความสามารถพิเศษในการควบคุมอุณหภูมิภายในตู้สินค้าของ ​Reefer Container มาพร้อมกับค่าใช้จ่ายพิเศษ นั่นก็​คือ ค่าไฟ ​Electricity Fee

​สำหรับ shipment นำเข้าสินค้าทั่วไป โดยปกติหากผู้ประกอบการไม่ดำเนินการเคลียร์สินค้าภายในระยะเวลา Free time ตามกำหนดที่สายเรือให้ไว้ ก็จะทำให้เกิดค่าใช้จ่ายที่เรียกว่า ค่าเสียเวลาตู้ (demurrage charge) ​​แต่ถ้ายิ่งเป็นตู้ ​Reefer Container​ ​แล้วจะต้องใช้ไฟฟ้าในการรักษาอุณหภูมิ จึงมีการเรียกเก็บค่าไฟเพิ่มอีกด้วย​

โดยประมาณก็จะอยู่ราว ๆ 600-1,500 บาทต่อวัน ​ซึ่ง​ปก​ติ​ สายเรือจะให้ฟรี Electricity fee 2-3 วัน แต่บางสายเรือก็ให้แค่เพียง 1 วันเท่านั้น!​

ดังนั้น shipment ที่ใช้ตู้ควบคุมอุณหภูมิควรจะต้องเช็คค่าใช้จ่ายเหล่านี้ให้ดี ๆ และรีบ​เคลียร์สินค้า​ออกจากท่าให้ไวที่สุดจะดีกว่า​นอกจากนี้ ​Reefer Container ตู้เย็นขนาดใหญ่ทีก็มีโอกาสที่จะเสียได้เป็นปกติ แม้โอกาสจะน้อยก็ตามที หรือแม้แต่ความเสี่ยงเรื่องการสลับกันใช้ปลั๊ก ในกรณีที่ปลั๊กไม่เพียงพอก็อาจจะทำให้สินค้าเสียหายได้ ดังนั้นผู้ประกอบการก็ควรจะต้องทำประกันภัยไว้ด้วย

ที่มา  [#TheLogistics](https://www.facebook.com/hashtag/thelogistics?source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARCm2nnOHV0MKjonKl_LXkgA7c2pi32i3yikqs5HClWu-DIQCq0M9gXdCqAZUOrgCYWqQJ_EP96N7J7D1AMdIpS_D4blF81Nsw9akpCzfYOZneRhgVL8hnqMbCrJ5P2YjTJVZpVUc5r-y-VGtTrD2R882qOfRbTGKs4zyozUC5CN7fBDMWn59P8wdAe3NC-8eoun76Rjb69wihKhX_17ZRRaIE7FK9rNcKQp6n-PCSEoVUusQYALUV2zQzWhCnyl6pab7iUIWZw2pLoiIeCbCUFJs5WBLVZExHICvbmGIwbYBQ2Q0Jx8mX3ty4LidppNpzdL7SCp1ritn-eiG_GXhzt8hw&__tn__=%2ANK-R)

[](https://www.facebook.com/tariffonline/photos/a.816486615107775/2871356616287421/?type=3&eid=ARCttVgN3gq5UYSMqG0m4gK4OI6VpL9_aI-JlXHPMwscNeJ5sxZv-tPr7N4GJPq9AG4mkWedmSJ91vLz&__xts__%5B0%5D=68.ARCm2nnOHV0MKjonKl_LXkgA7c2pi32i3yikqs5HClWu-DIQCq0M9gXdCqAZUOrgCYWqQJ_EP96N7J7D1AMdIpS_D4blF81Nsw9akpCzfYOZneRhgVL8hnqMbCrJ5P2YjTJVZpVUc5r-y-VGtTrD2R882qOfRbTGKs4zyozUC5CN7fBDMWn59P8wdAe3NC-8eoun76Rjb69wihKhX_17ZRRaIE7FK9rNcKQp6n-PCSEoVUusQYALUV2zQzWhCnyl6pab7iUIWZw2pLoiIeCbCUFJs5WBLVZExHICvbmGIwbYBQ2Q0Jx8mX3ty4LidppNpzdL7SCp1ritn-eiG_GXhzt8hw&__tn__=EHH-R)

![ไม่มีคำอธิบายรูปภาพ](https://scontent.fbkk2-7.fna.fbcdn.net/v/t1.0-0/p526x296/90484922_2871356619620754_5987034584984846336_n.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_ohc=bK_deyah4ukAX8d31Q7&_nc_ht=scontent.fbkk2-7.fna&_nc_tp=6&oh=5dbd48409e34f04ff1bde86477bcc746&oe=5E9F6124)


> [Source : ](https://www.facebook.com/tariffonline/photos/a.816486615107775/2871356616287421/?type=3&theater).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTM5MjI0MjAwOF19
-->