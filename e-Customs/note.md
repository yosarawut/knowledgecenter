

## แหล่งความรู้

-  [http://www.chanidservice.com/](http://www.chanidservice.com/)
- [https://greedisgoods.com/](https://greedisgoods.com/)
- [https://www.logisticafe.com/](https://www.logisticafe.com/)
- [https://slideplayer.in.th/](https://slideplayer.in.th/)


## Downloads

- [คู่มือพิกัดศุลกากรและรหัสสถิติมาตรฐานผลิตภัณฑ์อุตสาหกรรมที่มีพระราชกฤษฎีกากำหนดให้ต้องเป็นไปตามมาตรฐาน](http://appdb.tisi.go.th/tis_dev/file_share/stdinfo/manual_112TIS_21012019.pdf)
<!--stackedit_data:
eyJoaXN0b3J5IjpbODAwNzk0NzA4LDMwMDc1Njg3MCwtMjAxNz
YzODc0Nyw2ODk4MjI5NTEsLTM2NzY1MDg5LC0yNDQzMDMyMDcs
LTExNjIwMDM2NTEsODg0NTAxMTgxLDk2MjQ1OTg1M119
-->